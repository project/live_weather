# Live Weather

- A simple module provides a customized live weather
  report as a block using Yahoo weather API.

## Table of contents

- Introduction
- Requirements
- Installation
- Configuration
- Maintainers

## Introduction

- A simple module provides a customized live weather
  report as a block using Yahoo weather API.
- Enter Where On Earth IDentification (WOEID) of location
  to use this url (http://woeid.rosselliot.co.nz,)
  will get the location details.

## Requirements

- This module requires no modules outside of Drupal core.

## Installation

- Install this module as you would normally install
  a contributed Drupal module.
  [Visit](https://www.drupal.org/node/1897420) for further information.

## Configuration

- Once the module has been installed, navigate to
  admin/config/services/live_weather/location
  (Configuration > Web Services > Live Weather through the administration panel)
  and configure the locations.

## Maintainers
- Manikaprasanth Palanisamy - [manikaprasanth](https://www.drupal.org/u/manikaprasanth)
- Rajan Kumar - [rajan-kumar](https://www.drupal.org/u/rajan-kumar)
- Sergei Semipiadniy - [sergei_semipiadniy](https://www.drupal.org/u/sergei_semipiadniy)


